//
//  Massage.swift
//  BreakPoint
//
//  Created by Alaa khalil on 3/11/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import Foundation

class Massage {
    
    private var _content: String
    private var _senderID: String
    
    var content: String{
        return _content
    }
    
    var senderID: String{
        return _senderID
    }
    
    
    init(content: String, senderID: String) {
        self._content = content
        self._senderID = senderID
    }
}
