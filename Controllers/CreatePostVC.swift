//
//  CreatePostVC.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/26/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import UIKit
import Firebase

class CreatePostVC: UIViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileImage: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        textView.delegate = self
        self.sendBtn.bindToKeyboard()
       // self.textView.bindToKeyboard()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.emailLabel.text = Auth.auth().currentUser?.email
    }
    
    @IBAction func sendBtn(_ sender: Any) {
        if textView.text != nil && textView.text != "Say something here..."{
        sendBtn.isEnabled = false
            DataService.instance.uploadPost(withMassage: textView.text, forUID: (Auth.auth().currentUser?.uid)!, withGroupKey: nil) { (isComplete) in
                
                if isComplete{
                    self.sendBtn.isEnabled = true
                    self.dismiss(animated: true, completion: nil)
                }
                else{
                    self.sendBtn.isEnabled = true
                    print("there was an error with upload post! ")
                }
            }
            
        }
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
  
}

extension CreatePostVC: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
}
