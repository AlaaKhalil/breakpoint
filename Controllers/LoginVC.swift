//
//  LoginVC.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/25/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var password: InsetTextField!
    @IBOutlet weak var email: InsetTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        email.delegate = self
        password.delegate = self
    }
    
    @IBAction func signInBtn(_ sender: Any) {
        if email.text != nil && password.text != nil {
            AuthService.instance.loginUser(withEmail: email.text!, andPassword: password.text!) { (success, loginError) in
                if success{
                    self.dismiss(animated: true, completion: nil)
                }
                else{
                    print(String(describing: loginError?.localizedDescription))
                }
                AuthService.instance.registerUser(withEmail: self.email.text!, andPassword: self.password.text!, userCreationComplete: { (success, registerError) in
                    if success{
                        AuthService.instance.loginUser(withEmail: self.email.text!, andPassword: self.password.text!, userLoginComplete: { (success, nil) in
                            if success{
                                self.dismiss(animated: true, completion: nil)
                                print("successfully register user")
                            }
                           
                        })
                    }
                        
                    else{
                        print(String(describing: registerError?.localizedDescription))
                    }
                })
            }
        }
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    

}
extension LoginVC: UITextFieldDelegate{
    
}
