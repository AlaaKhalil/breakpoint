//
//  FirstViewController.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/17/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import UIKit

class FeedVC: UIViewController {

    var massagesArray = [Massage]()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DataService.instance.getAllFeedMassage { (returnMassages) in
            self.massagesArray = returnMassages.reversed()
            self.tableView.reloadData()
        }
    }


}
extension FeedVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return massagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "feedCell") as? FeedCell
            else{ return UITableViewCell() }
        let image = UIImage(named: "defaultProfileImage")
        let massages = massagesArray[indexPath.row]
        DataService.instance.getUserName(forUID: massages.senderID) { (email) in
            cell.confogureCell(profileImage: image!, email: email , content: massages.content)
            
        }
        
        return cell
    }
 
}

