//
//  MeVC.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/26/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import UIKit
import Firebase

class MeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.emailLabel.text = Auth.auth().currentUser?.email
    }
    
    @IBAction func signOutBtn(_ sender: Any) {
        
        let logoutPopup = UIAlertController(title: "logout?", message: "Are you sure you want to logout ??", preferredStyle: .actionSheet)
        let logoutAction = UIAlertAction(title: "logout?", style: .destructive) { (buttonTapped) in
            
            do{
                try Auth.auth().signOut()
                let loginVc = self.storyboard?.instantiateViewController(withIdentifier: "AuthVC") as? AuthVC
                self.present(loginVc!, animated: true, completion: nil)
            }
            catch{
                print(error)
            }
            
        }
        logoutPopup.addAction(logoutAction)
        present(logoutPopup, animated: true, completion: nil)
    }
   
}
