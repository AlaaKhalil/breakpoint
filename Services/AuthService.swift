//
//  AuthService.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/25/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import Foundation
import Firebase

class AuthService {
    
    static let instance = AuthService()
    
    func registerUser(withEmail email: String, andPassword password: String, userCreationComplete: @escaping (_ status: Bool, _ error: Error?)->()){
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user else{
                userCreationComplete(false, error)
                return
            }
            let userData = ["provider": user.user.providerID, "email": user.user.email]
            DataService.instance.createUser(uid: user.user.uid, userData: userData as Dictionary<String, Any>)
            userCreationComplete(true, nil)
        }
        
    }
    func loginUser(withEmail email: String, andPassword password: String, userLoginComplete: @escaping (_ status: Bool, _ error: Error?)->()){
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                userLoginComplete(false, error)
                return
            }
            userLoginComplete(true, nil)
        }
        
    }
}
