//
//  DataService.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/17/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import Foundation
import Firebase

let DB_Base = Database.database().reference()


class DataService {
    
    static let instance = DataService()
    
    private var _REF_BASE = DB_Base
    private var _REF_USERS = DB_Base.child("users")
    private var _REF_GROUPS = DB_Base.child("groups")
    private var _REF_FEEDS = DB_Base.child("feeds")
    
     var REF_BASE: DatabaseReference{
        return _REF_BASE
    }
     var REF_USERS : DatabaseReference{
        return _REF_USERS
    }
     var REF_GROUPS: DatabaseReference {
        return _REF_GROUPS
    }
     var REF_FEED: DatabaseReference{
        return _REF_FEEDS
    }
    
    // create firebase user
    func createUser(uid: String, userData: Dictionary<String, Any>){
        
        REF_USERS.child(uid).updateChildValues(userData)
    }
    
    // get username & email from userId
    func getUserName(forUID userID: String, handler: @escaping (_ userName: String) -> ()) {
        
        REF_USERS.observeSingleEvent(of: .value) { (userSnapShot) in
            // return all data of REF_USERS
            guard let snapShot = userSnapShot.children.allObjects as? [DataSnapshot] else{return}
            for user in snapShot{
                if user.key == userID{
                    // passed the user called email
                    handler(user.childSnapshot(forPath: "email").value as! String)
                }
            }
        }
        
    }
    
    // upload post
    func uploadPost(withMassage massage: String, forUID uid: String, withGroupKey groupKey: String?, sendCompletion: @escaping(_ status: Bool) ->()){
        if groupKey != nil{
            // send to groups ref
        }
        else{
            //send to public feed
            REF_FEED.childByAutoId().updateChildValues(["content" : massage, "senderID": uid])
            sendCompletion(true)
        }
        
    }
    
    func getAllFeedMassage(handler: @escaping (_ massage: [Massage]) -> ()){
        
        var massageArray = [Massage]()
        REF_FEED.observeSingleEvent(of: .value) { (feeedMassage) in
            guard let feeedMassage = feeedMassage.children.allObjects as? [DataSnapshot] else {return}
            for massage in feeedMassage{
              let content = massage.childSnapshot(forPath: "content").value as! String
                let senderId = massage.childSnapshot(forPath: "senderID").value as! String
                let massage = Massage(content: content, senderID: senderId)
                massageArray.append(massage)
            }
            handler(massageArray)
        }
        
    }
    
    func getEmail(forSearchquery query: String ,handler: @escaping (_ emailAray: [String])->()){
        var emailArray = [String]()
        REF_USERS.observeSingleEvent(of: .value) { (emailSnapShot) in
            guard let snapShot = emailSnapShot.children.allObjects as? [DataSnapshot] else{return}
            for data in snapShot {
                let email = data.childSnapshot(forPath: "email").value as! String
                // check if email contains the charcter in search
                if email.contains(query) == true && email != Auth.auth().currentUser?.email {
                    emailArray.append(email)
                }
            }
            handler(emailArray)
        }
        
    }
    
   
    
}
