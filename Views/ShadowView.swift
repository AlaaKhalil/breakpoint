//
//  ShadowView.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/25/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import UIKit

class ShadowView: UIView {

    override func awakeFromNib() {
        self.setUpView()
        super.awakeFromNib()
    }
    func setUpView(){
        self.layer.shadowOpacity = 0.75
        self.layer.shadowRadius = 5
        self.layer.shadowColor = UIColor.black.cgColor
    }

}
