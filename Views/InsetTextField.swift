//
//  InsetTextField.swift
//  BreakPoint
//
//  Created by Alaa khalil on 2/25/19.
//  Copyright © 2019 mahmoud farid. All rights reserved.
//

import UIKit

class InsetTextField: UITextField {
    
    
    private var padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    
    override func awakeFromNib() {
        self.setUpView()
        super.awakeFromNib()
    }
    
   override func textRect(forBounds bounds: CGRect) -> CGRect {
    return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)

    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)

    }
    
    func setUpView(){
        let placeHolder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
        self.attributedPlaceholder = placeHolder
    }

}
